# **MATLAB scripts for the numerical examples in the book: A. Varga, Solving Fault Detection Problems, Springer, 2017. **  #


## About 

`FDIBOOK_EXAMPLES` is a collection of MATLAB scripts developed in conjunction with the book

   -  Andreas Varga, [Solving Fault Diagnosis Problems - Linear Synthesis Techniques](http://www.springer.com/us/book/9783319515588), vol. 84 of Studies in Systems, Decision and Control, Springer International Publishing, xxviii+394, 2017.

It comprises all codes listed in the book, several alternative codes which are not listed, 
but mentioned in the book or represent new alternative codes based on recent implementations of the 
synthesis methods, and new codes which address other examples in the book for which, originally, no code was provided.   

The current release of the `FDIBOOK_EXAMPLES` is version V1.0, dated November 30, 2018.


## Requirements

The codes have been developed under MATLAB 2015b and have been tested with MATLAB 2016a through 2017b. To execute the scripts, the Control System Toolbox, the Robust Control Toolbox, the Descriptor System Tools collection - [`DSTOOLS`](https://bitbucket.org/DSVarga/dstools) (Version V0.71 or later),
and the Fault Detection and Isolation Tools collection - [`FDITOOLS`](https://bitbucket.org/DSVarga/fditools) (Version V1.0 or later), must be installed in MATLAB running under 64-bit Windows 7, 8, 8.1 or 10. 

## License

See `license.txt` for licensing information.

## Related Links

The original list of MATLAB files corresponding to the version V0.2 of FDITOOLOS, together with instructions for their installation are available [here](https://sites.google.com/site/andreasvargacontact/home/book/matlab). 

The current list of MATLAB files and instructions for their installation are available [here](https://sites.google.com/site/andreasvargacontact/home/software/fdibook_examples). 
